package main

import (
	"fmt"

	"github.com/tobischo/gokeepasslib"

	"os"
)

func logEntry(e gokeepasslib.Entry) {
	var s string
	for _, data := range e.Values {
		s += fmt.Sprintf("k: %s, v: %s", data.Key, data.Value.Content)
		s += "\n"
	}
	logit(s)
}

func logit(s string) {
	filename := "/tmp/keepass-cli.log"
	f, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		return
	}

	defer f.Close()

	if _, err = f.WriteString(s); err != nil {
		return
	}
}
