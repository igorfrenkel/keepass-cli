package main

import (
	"fmt"
	"log"

	"github.com/rivo/tview"
	"github.com/tobischo/gokeepasslib"

	"os"
)

func entryData(e gokeepasslib.Entry) string {
	logEntry(e)
	var url, title, uname, pass string
	for _, data := range e.Values {
		switch data.Key {
		case "Title":
			title = data.Value.Content
		case "URL":
			url = data.Value.Content
		case "Password":
			pass = data.Value.Content
		case "UserName":
			uname = data.Value.Content
		}
	}
	return fmt.Sprintf("%s: url=%s (u=%s, p=%s)", title, url, uname, pass) // protected?
}

func walk(gnode *tview.TreeNode, group gokeepasslib.Group) {
	for _, e := range group.Entries {
		add(gnode, entryData(e))
	}
	for _, gg := range group.Groups {
		n := add(gnode, gg.Name)
		walk(n, gg)
	}

}

func startWalk(root *tview.TreeNode, kroot gokeepasslib.RootData) {
	for _, gg := range kroot.Groups {
		n := add(root, gg.Name)
		walk(n, gg)
	}
}

func loadkeepass(root *tview.TreeNode) error {
	file, err := os.Open("./example.kdbx")
	if err != nil {
		log.Fatalf("failed to open keepass file termui: %v", err)
	}

	db := gokeepasslib.NewDatabase()
	db.Credentials = gokeepasslib.NewPasswordCredentials("foobarbaz")
	err = gokeepasslib.NewDecoder(file).Decode(db)
	if err != nil {
		log.Fatalf("failed to initialize keepass db: %v", err)
	}
	db.UnlockProtectedEntries()

	startWalk(root, *db.Content.Root)
	return nil
}
