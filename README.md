# keepass cli

## design

- root
  - work
  - home
    - wifi
      - ap 2.4 | k_g_b / x1yz <- 
      - ap 3.5
  - office

## TODO

### MVC copy functionality

- [x] tree view
- [x] open keepass db
- [x] pt1 render keepass db under tree view
- [x] pt2 render keepass in a basic format with usr, pass, url
- [x] side-by-side view of same db
- [x] switch between sides
- [x] debug ui application through remote connection (debugger doesn't work
  after render)
- [x] multi-select on side 1
- [x] go to side 2 and cp selected entries
- [ ] remove selected entires from both trees 
- [ ] save new kdbx
- [ ] verify move scenarios
- [ ] refactor

### Usability
- [ ] treenode for values of entry attributes
- [ ] keepass db password prompt
- [ ] keepass db password prompt
- [ ] bind key C and copy to clipboard on key-press
