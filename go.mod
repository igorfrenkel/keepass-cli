module gitlab.com/igorfrenkel/keecliapp

go 1.18

require (
	github.com/gdamore/tcell/v2 v2.5.3
	github.com/rivo/tview v0.0.0-20221029100920-c4a7e501810d
	github.com/tobischo/gokeepasslib v1.0.0
)

require (
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/rivo/uniseg v0.4.2 // indirect
	golang.org/x/sys v0.0.0-20220318055525-2edf467146b5 // indirect
	golang.org/x/term v0.0.0-20210220032956-6a3ed077a48d // indirect
	golang.org/x/text v0.3.7 // indirect
)
